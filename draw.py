import turtle
t = turtle.Pen()
COLORS = ['red', 'green', 'blue', 'purple', 'orange', 'yellow', 'pink', 'cyan', 'magenta', 'AliceBlue', 'aquamarine', 'azure', 'CadetBlue']
sides = 6
turtle.bgcolor("black")
t.speed(0)
sides = int(turtle.textinput('Enter the number of the shape\'s sides', 'How many sides to do you want?'))

offset = float(turtle.textinput('Offset degrees', 'By how many degrees do you want to turn the shape?'))

width = turtle.textinput('Line Width', 'How thick is the line? (in pixels)')

for x in range(1000):
    t.pencolor(COLORS[x%int(sides)])
    t.forward(x)
    t.left(360 / sides  + float(offset))
    t.width(width)


    
