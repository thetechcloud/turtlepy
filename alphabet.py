import engine
engine.start()
def a(multiplier):
	engine.t.left(90)
	engine.t.forward(4 * multiplier)
	engine.t.right(45)
	engine.t.forward(2 * multiplier)
	engine.t.right(90)
	engine.t.forward(2 * multiplier)
	engine.t.right(45)
	engine.t.forward(4 * multiplier)
	engine.t.left(180)
	engine.t.forward(2 * multiplier)
	engine.t.left(90)
	engine.t.forward(3 * multiplier)
