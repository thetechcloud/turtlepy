# Always initialize turtle with start() before using this module!
import turtle
import string



def draw_poly(pwidth, poffset, plength, psides):
    for x in range(psides):
        t.width(pwidth)
        t.forward(plength)
        t.left((360 / psides) + float(poffset))



def start():
    global t
    t = turtle.Pen()

def setpos(x,y):
    t.penup()
    t.setx(x)
    t.sety(y)
    t.pendown()
