# Rules of turtlepy repo:

1. When making a new turtle design, please create a new script with a representative name.

2. Remember: engine.py is there to be used, keep it stable and try to utilize it, thus simplifying our code.

3. If you plan to make a large change to a script or a change of function in a script, make sure to create a new branch and ask to have it merged back once you're happy with your changes.

3a. Before you request a merge of your branch, make sure you test it, and keep the master branch stable.

